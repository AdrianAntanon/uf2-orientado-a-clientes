var map = L.map('map').setView([41.39577, 2.14829], 13);
L.tileLayer('https://api.maptiler.com/maps/streets/{z}/{x}/{y}.png?key=rLrIWv6QRXTJu2nodK7n', {
  tileSize: 512,
  zoomOffset: -1,
  minZoom: 1,
  attribution: "\u003ca href=\"https://www.maptiler.com/copyright/\" target=\"_blank\"\u003e\u0026copy; MapTiler\u003c/a\u003e \u003ca href=\"https://www.openstreetmap.org/copyright\" target=\"_blank\"\u003e\u0026copy; OpenStreetMap contributors\u003c/a\u003e",
  crossOrigin: true
}).addTo(map);
var marker = L.marker([41.4529533, 2.1865433]).addTo(map);

let data = null;

let xhr = new XMLHttpRequest();

xhr.addEventListener("readystatechange", function () {
  if (this.readyState === this.DONE) {

    let myArray = JSON.parse(this.responseText);

    const records = myArray.result.records;

    let myListOfBikes = [];

    records.filter((bike) => myListOfBikes.push([bike.streetName, bike.latitude, bike.longitude, bike.bikes]))

    myListOfBikes = myListOfBikes.sort((bikeA, bikeB) => bikeA[3] - bikeB[3]);

    let circleUpBicingAreas = function(latitude, longitude, color, fillColor){
      L.circle([latitude, longitude], {
        color: color,
        fillColor: fillColor,
        fillOpacity: 0.5,
        radius: 150
      }).addTo(map);
    }

    myListOfBikes.forEach((bike) => {
      let numberOfBikes = bike[3];
      let latitude = bike[1];
      let longitude = bike[2];

      if (numberOfBikes < 5) circleUpBicingAreas(latitude, longitude, 'green', '#69FF5B');
      else if (numberOfBikes < 10) circleUpBicingAreas(latitude, longitude, 'red', '#f03');
      else if (numberOfBikes < 20) circleUpBicingAreas(latitude, longitude, 'yellow', '#EBDA43');
      else circleUpBicingAreas(latitude, longitude, 'blue', '#5386F9');
    })


    console.table(myListOfBikes);

  }
});

let url = 'http://bicing.puigverd.org/data/api/action/datastore_search?resource_id=f59e276c-1a1e-4fa5-8c89-8a8a56e56b34';

xhr.open("GET", url);

xhr.send(data);


