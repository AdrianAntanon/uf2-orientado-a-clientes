
class RecordingCompany {
    storage = [];

    constructor() { }

    getRCInfo() {
        const info = this.storage.length === 0 ? 'No hay ningún disco en la colección' : this.storage;
        return info;
    }

    addDisc(disc) {
        this.storage.push(disc.getDiscInfo());
    }

    deleteDisc(deleteAlbum) {
        deleteAlbum = deleteAlbum.getDiscInfo();
        this.storage.filter((disc) => {
            if (disc.Album === deleteAlbum.Album && disc.Artist === deleteAlbum.Artist && disc.ReleaseYear === deleteAlbum.ReleaseYear) {
                let position = this.storage.indexOf(disc);
                this.storage.splice(position, 1)
            }
        })
    }

    getCollectionSize() {
        return this.storage.length;
    }

    sortCollection() {
        let checkSizeOfDiscCollection = this.getCollectionSize();
        if (checkSizeOfDiscCollection > 1) {
            this.storage.sort((artistA, artistB) => artistA.ReleaseYear - artistB.ReleaseYear)
        } else {
            console.log('Solo hay 1 disco, no se puede ordenar nada');
        }
    }

    filerCollection(musicGenre, genreSelected) {
        let genreSelectedCapital = genreSelected.charAt(0).toUpperCase()+genreSelected.slice(1);

        console.log(genreSelectedCapital);

        const filteredDiscCollection = this.storage.filter((disc) => {
            let keysOfDisc = Object.keys(disc);
            if (keysOfDisc[4] === musicGenre) {
                if (disc.Genre === genreSelectedCapital) {
                    return disc;
                }
            }
        });
        return filteredDiscCollection;
    }

    listCollection() {
        this.storage.map((disc) => {
            let valuesWithoutCover = Object.values(disc);
            valuesWithoutCover.pop();
            const newDisc = {
                Album: valuesWithoutCover[0],
                Artist: valuesWithoutCover[1],
                Formed: valuesWithoutCover[2],
                ReleaseYear: valuesWithoutCover[3],
                Genre: valuesWithoutCover[4],
                Shelving: valuesWithoutCover[5],
                Borrowed: valuesWithoutCover[6]
            };
            console.log(newDisc);
        })
    }
}

class Performer {
    #name;
    #yearOfBirth;

    constructor(name, yearOfBirth) {
        this.#name = name;
        this.#yearOfBirth = yearOfBirth;
    }

    getPerformerInfo() {
        let infoArtist = [`${this.#name}`, this.#yearOfBirth];
        return infoArtist;
    }

}

class Disc {
    #albumName;
    #performer;
    #releaseYear;
    #musicGenre;
    #location;
    #borrowed;
    #url;

    constructor() {
        this.#albumName = '';
        this.#performer = '';
        this.#releaseYear = '';
        this.#musicGenre = null;
        this.#location = 0;
        this.#borrowed = false;
        this.#url = null;
    }

    getDiscInfo() {
        let borrowedDisco = this.#borrowed ? 'Yes' : 'No';
        const performerBand = this.#performer.getPerformerInfo()

        let discInfo = {
            Album: this.#albumName,
            Artist: performerBand[0],
            Formed: performerBand[1],
            ReleaseYear: this.#releaseYear,
            Genre: this.#musicGenre,
            Shelving: this.#location,
            Borrowed: borrowedDisco,
            Cover: this.#url
        }
        return discInfo;
    }

    setFullDisc(albumName, performer, releaseYear, location, genre) {
        const options = ['Rock', 'Pop', 'Punk', 'Indie']
        const optionsToLowerCase = options.map(option => option.toLowerCase());

        this.#albumName = albumName;
        this.#performer = new Performer(performer[0], performer[1]);
        this.#releaseYear = releaseYear;
        this.#location = location;

        switch (genre.toLowerCase()) {
            case optionsToLowerCase[0]:
                this.#musicGenre = options[0];
                break;
            case optionsToLowerCase[1]:
                this.#musicGenre = options[1];
                break;
            case optionsToLowerCase[2]:
                this.#musicGenre = options[2];
                break;
            case optionsToLowerCase[3]:
                this.#musicGenre = options[3];
                break;
            default:
                this.#musicGenre = null;
                break;
        }
    }

    setUrl(url) {
        this.#url = url;
    }

    setLocation(newLocation) {
        this.#location = newLocation;
    }

    setBorrowed(boolean) {
        this.#borrowed = boolean;
    }
}

const painkiller = new Disc();
painkiller.setFullDisc('Painkiller', ['Judas Priest', 1969], 1990, 3, 'rock');
const painkillerURL = 'https://i2.wp.com/www.scienceofnoise.net/wp-content/uploads/2020/04/painkiller-pic.jpg';
painkiller.setBorrowed(true);
painkiller.setUrl(painkillerURL);

const rideTheLightning = new Disc();
rideTheLightning.setFullDisc('Ride The Lightning', ['Metallica', 1981], 1984, 3, 'rock')
const rideTheLightningURL = 'https://images-na.ssl-images-amazon.com/images/I/71jyH5WtmFL._SL1425_.jpg';
rideTheLightning.setUrl(rideTheLightningURL)

const jomsviking = new Disc()
jomsviking.setFullDisc('Jomsviking', ['Amon Amarth', 1992], 2016, 3, 'rock');
const jomsvikingURL = 'https://images-na.ssl-images-amazon.com/images/I/81Y1CR23RLL._SL1000_.jpg'
jomsviking.setUrl(jomsvikingURL);

const theQueenIsDead = new Disc();
theQueenIsDead.setFullDisc('The Queen Is Dead', ['The Smiths', 1982], 1986, 2, 'Indie')
const theQueenIsDeadURL = 'https://images-na.ssl-images-amazon.com/images/I/51Bgnrde4OL.jpg';
theQueenIsDead.setUrl(theQueenIsDeadURL);

const sehnsucht = new Disc();
sehnsucht.setFullDisc('Sehnsucht', ['Rammstsein', 1994], 1997, 3, 'rock')
const sehnsuchtURL = 'https://images-na.ssl-images-amazon.com/images/I/61hGJLFbkkL._SX425_.jpg';
sehnsucht.setUrl(sehnsuchtURL);

const loveOfLesbian1999 = new Disc();
loveOfLesbian1999.setFullDisc('1999 (o cómo generar incendios de nieve con una lupa enfocando a la luna)', ['Love of Lesbian', 1997], 2009, 2, 'Indie');
const loveOfLesbian1999URL = 'https://www.elquintobeatle.com/wp-content/uploads/2016/03/love-of-lesbian-1999-o-como-generar-incendios-de-nieve-con-una-lupa-enfocada-a-la-luna-1.jpg'
loveOfLesbian1999.setUrl(loveOfLesbian1999URL);

const laGuapaYLosNinjas = new Disc();
laGuapaYLosNinjas.setFullDisc('La guapa y los Ninjas', ['Los Ganglios', 2009], 2012, 1, 'pop');
const laGuapaYLosNinjasURL = 'https://m.media-amazon.com/images/I/81NeuLgVGUL._SS500_.jpg';
laGuapaYLosNinjas.setUrl(laGuapaYLosNinjasURL);

const powerslave = new Disc();
powerslave.setFullDisc('Powerslave', ['Iron Maiden', 1975], 1984, 3, 'rock');
const powerslaveURL = 'https://storefeederimages.blob.core.windows.net/eyesoremerch/Products/59fbccbe-1cab-4760-94c0-03ba5a1f2da1/Full/z3rhm5waq50.jpg';
powerslave.setUrl(powerslaveURL);

const lifeIsKillingMe = new Disc();
lifeIsKillingMe.setFullDisc('Life Is Killing Me', ['Type 0 Negative', 1989], 2000, 3, 'rock');
const lifeIsKillingMeURL = 'https://images-na.ssl-images-amazon.com/images/I/7198JzLDttL._SL1422_.jpg';
lifeIsKillingMe.setUrl(lifeIsKillingMeURL);

const mismoSitioDistintoLugar = new Disc();
mismoSitioDistintoLugar.setFullDisc('Mismo sitio, distinto lugar', ['Vetusta Morla', 1998], 2017, 1, 'pop');
const mismoSitioDistintoLugarURL = 'https://www.fonodisco.es/234350-large_default/vetusta-morla-mismo-sitio-distinto-lugar-cd-.jpg';
mismoSitioDistintoLugar.setUrl(mismoSitioDistintoLugarURL);

const elAngelCaido = new Disc();
elAngelCaido.setFullDisc('El Angel Caído', ['Avalanch', 2001], 1988, 3, 'rock');
const elAngelCaidoURL = 'https://images-na.ssl-images-amazon.com/images/I/61Y%2B19ev9rL.jpg';
elAngelCaido.setUrl(elAngelCaidoURL);


const desintegration = new Disc();
desintegration.setFullDisc('Desintegration', ['The Cure', 1978], 1989, 4, 'punk');
const desintegrationURL = 'https://images-na.ssl-images-amazon.com/images/I/81XzjwEhhlL._SL1425_.jpg';
desintegration.setUrl(desintegrationURL);

const unknownPleasures = new Disc();
unknownPleasures.setFullDisc('Unknown Pleasures', ['Joy Division', 1976], 1979, 4, 'punk');
const unknownPleasuresURL = 'https://loopermx.com/wp-content/uploads/2019/06/joy.jpg';
unknownPleasures.setUrl(unknownPleasuresURL);

const ramones = new Disc();
ramones.setFullDisc('Ramones', ['Ramones', 1974], 1976, 4, 'punk');
const ramonesURL = 'https://images-na.ssl-images-amazon.com/images/I/A1dVF2DBaxL._SL1500_.jpg';
ramones.setUrl(ramonesURL);

const horrorEnElHipermercado = new Disc();
horrorEnElHipermercado.setFullDisc('Horror en el hipermercado', ['Alaska y los pegamoides', 1980], 1979, 1, 'pop');
const horrorEnElHipermercadoURL = 'https://lafonoteca.net/wp-content/uploads/2008/01/alaska_y_los_pegamoides_5001.jpg';
horrorEnElHipermercado.setUrl(horrorEnElHipermercadoURL);

const musicaModerna = new Disc();
musicaModerna.setFullDisc('Música moderna', ['Radio Futura', 1979], 1980, 1, 'pop');
const musicaModernaURL = 'https://images-na.ssl-images-amazon.com/images/I/811TUlcXcfL._SX355_.jpg';
musicaModerna.setUrl(musicaModernaURL);

const voces = new Disc();
voces.setFullDisc('Voces', ['El último vecino', 2013], 2016, 2, 'indie');
const vocesURL = 'https://f4.bcbits.com/img/0006961490_10.jpg';
voces.setUrl(vocesURL);

const alfa = new Disc();
alfa.setFullDisc('Alfa', ['WarCry', 2002], 2011, 3, 'rock');
const alfaURL = 'https://www.warcry.es/wc-contenido/uploads/2014/10/Warcry-alfa.jpg';
alfa.setUrl(alfaURL);

const laLeyendaDeLaMancha = new Disc();
laLeyendaDeLaMancha.setFullDisc('La leyenda de la mancha', ['Mago de Oz', 1988], 1998, 3, 'rock');
const laLeyendaDeLaManchaURL = 'https://i.ytimg.com/vi/46nIvrK4pSg/hqdefault.jpg';
laLeyendaDeLaMancha.setUrl(laLeyendaDeLaManchaURL);

const laLeyInnata = new Disc();
laLeyInnata.setFullDisc('La ley innata', ['Extremoduro', 2008], 1987, 3, 'rock');
const laLeyInnataURL = 'https://www.elquintobeatle.com/wp-content/uploads/2015/02/extremoduro-la-ley-innata-1.jpg'
laLeyInnata.setUrl(laLeyInnataURL);

const parchis = new Disc();
parchis.setFullDisc('Las 25 Super Canciones de los Peques', ['Parchis', 1979], 1979, 1, 'pop');
const parchisURL = 'https://images-na.ssl-images-amazon.com/images/I/71Jw3--g8sL._SX355_.jpg';
parchis.setUrl(parchisURL);


const discCollection = [
    painkiller,
    rideTheLightning,
    jomsviking,
    theQueenIsDead,
    sehnsucht,
    loveOfLesbian1999,
    laGuapaYLosNinjas,
    powerslave,
    lifeIsKillingMe,
    mismoSitioDistintoLugar,
    elAngelCaido,
    desintegration,
    unknownPleasures,
    ramones,
    horrorEnElHipermercado,
    musicaModerna,
    voces,
    alfa,
    laLeyendaDeLaMancha,
    laLeyInnata,
    parchis
];

let collection = new RecordingCompany();

discCollection.forEach(disc => collection.addDisc(disc));


console.log('Ordena la colección por fecha de lanzamiento del album')
collection.sortCollection();


console.log('Borrado disco Painkiller de Judas Priest')
collection.deleteDisc(painkiller);


console.log('Colección filtrada por el género pop')
console.log(collection.filerCollection('Genre', 'pop'));


console.log('Muestra por terminal la colección sin el url de la portada')
collection.listCollection();