Explicación detallada de por qué no funciona bicingHtml.js si no es con el segundo método.

El problema es generado debido que la API del ayuntamiento es privada, por lo que no estamos dentro de la lista de permisos que recibe Access-Control-Allow-Origin.

Ese error viene dado por un mecanismo de seguridad que los navegadores llevan implementado llamado same-origin policy, el cual sirve para evitar la ciber ataques
falsificando tu identidad

Según mi investigación he podido encontrar 3 supuestas formas de solucionar el problema y que me permita acceder a esos datos.

Primer método:
Instalar una extensión llamada Moesif Origin & CORS Changer, aunque solo nos serviría de manera local, es decir, allá donde tengamos instalada la extensión.
Por lo tanto, no es una buena solución, ya que es un parche.

Segundo método:
Enviar la petición a un proxy, para que el proxy actúe como intermediario entre cliente y servidor
Ejemplo: https://api-publica.com/https://opendata-ajuntament.barcelona.cat/data/api/action/datastore_search?resource_id=f59e276c-1a1e-4fa5-8c89-8a8a56e56b34

Por lo que se salta el permiso de control al tener la api-publica acceso a todo el mundo, tardará un poco en devolver los datos pero lo hará, en principio.

Tercer método:
Construir tu propio proxy donde le especifíques que tiene acceso todo el mundo
