const array = [1, 2, 3, 4];


const newArray = (newValue) => {
    let myNewArray = array;
    myNewArray.push(newValue);

    return myNewArray;
};

let example = newArray(10);

firstTest = example + 10;
secondTest = [...example] + 10;

console.log(firstTest);
console.log(secondTest);

// Hace exactamente lo mismo, interesante que haya más formas de llegar a ello.