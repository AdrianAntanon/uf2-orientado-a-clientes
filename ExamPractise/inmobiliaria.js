class Property {
    static #numberOfProperties = 0;
    #address;
    #m2;
    #cadastralReference;
    #coordinates;
    #isNew;
    #basePrice;
    #constructionDate;


    constructor(
        address,
        m2,
        cadastralReference,
        coordinates,
        isNew,
        basePrice,
        constructionDate
    ) {
        this.#address = address;
        this.#m2 = m2;
        this.#cadastralReference = cadastralReference;
        this.#coordinates = coordinates;
        this.#isNew = isNew;
        this.#basePrice = basePrice;
        this.#constructionDate = constructionDate;

        Property.#numberOfProperties += 1;
    }

    static getNumberOfProperties() {
        return Property.#numberOfProperties;
    }

    getInfo() {

        const obj = {
            address: this.getAddress(),
            m2: this.getM2(),
            cadastralReference: this.getCadastralReference(),
            coordinates: this.getCoordinates(),
            isNew: this.getIsNew(),
            price: this.getPrice(),
            yearsOfBuilding: this.getYearsOfTheProperty(),
        }

        return obj;
    }

    getAddress() {
        return this.#address;
    }

    getCadastralReference() {
        return this.#cadastralReference[0] + ", " + this.#cadastralReference[1];
    }

    getCoordinates() {
        return this.#coordinates;
    }

    getM2() {
        return this.#m2;
    }

    getIsNew() {
        return this.#isNew;
    }

    getYearsOfTheProperty() {
        const today = new Date().getFullYear();

        return today - this.#constructionDate;

    }

    getPrice() {
        let yearsOfTheProperty = this.getYearsOfTheProperty();

        let realPrice = this.#basePrice;

        if (yearsOfTheProperty > 10 && yearsOfTheProperty < 30) {
            let discount = 20;

            while (yearsOfTheProperty < 30) {
                yearsOfTheProperty++;
                discount--;
            }

            discount = (100 - discount) / 100;
            realPrice *= discount;
        } else if (yearsOfTheProperty > 30) {
            realPrice *= 0.80;
        }


        return realPrice;
    }
}


const MyApartment = new Property("Calle Joaquin Valls", 65, [123, 456], 124354, false, 150000, 1980);

// console.log(MyApartment.getPrice());


class Local extends Property {
    constructor(
        address,
        m2,
        cadastralReference,
        coordinates,
        isNew,
        basePrice,
        constructionDate
    ) {
        super(address,
            m2,
            cadastralReference,
            coordinates,
            isNew,
            basePrice,
            constructionDate)
    }


    getLocal() {
        return "Esto es un local";
    }
}


const localDePrueba = new Local("Calle Joaquin Valls", 65, [123, 456], 124354, false, 150000, 1980)

// console.log(Property.getNumberOfProperties());


console.log("getInfo nos devuelve lo siguiente\n", MyApartment.getInfo());