const request = require('request');

const url = 'https://opendata-ajuntament.barcelona.cat/data/api/action/datastore_search?resource_id=f59e276c-1a1e-4fa5-8c89-8a8a56e56b34';

let options = {
    method: 'GET',
    url: url
};
request(options, function(error, response, body){
    if(error) throw new Error(error);

    const myArray = JSON.parse(body);

    const records = myArray.result.records;

    let myListOfBikes = [];

    records.filter( (bike) =>{
        if(bike.bikes >= 10){
            myListOfBikes.push([bike.streetName, bike.latitude, bike.longitude, bike.bikes])
        }
    })

    myListOfBikes = myListOfBikes.sort((bikeA, bikeB) => bikeA[3] - bikeB[3]);

    console.log("Listado de paradas de Bicing con 10 o más bicis, ordenadas por número de disponibles de menor a mayor");

    console.table(myListOfBikes);
});