class Property {
    static #numberOfProperties = 0;
    #address;
    #m2;
    #cadastralReference;
    #coordinates;
    #isNew;
    #basePrice;
    #constructionDate;

    constructor(address, m2, cadastralReference, coordinates, isNew, basePrice, constructionDate) {
        this.#address = address;
        this.#m2 = m2;
        this.#cadastralReference = cadastralReference;
        this.#coordinates = coordinates;
        this.#isNew = isNew;
        this.#basePrice = basePrice;
        this.#constructionDate = constructionDate;
        Property.#numberOfProperties += 1;

    }

    static getNumberOfProperties() {
        return Property.#numberOfProperties;
    }

    getPrice() {
        let yearsOfTheProperty = this.getYearsOfTheProperty();

        let realPrice = this.#basePrice;

        if (yearsOfTheProperty > 10 && yearsOfTheProperty < 30) {
            let discount = 20;

            while (yearsOfTheProperty < 30) {
                yearsOfTheProperty++;
                discount--;
            }

            discount = (100 - discount) / 100;
            realPrice *= discount;
        } else if (yearsOfTheProperty > 30) {
            realPrice *= 0.80;
        }


        return realPrice;
    }

    getM2() {
        return this.#m2;
    }

    getIsNew() {
        return this.#isNew;
    }

    getYearsOfTheProperty() {
        const today = new Date().getFullYear();

        return today - this.#constructionDate;
    }
}

class Flat extends Property {
    static #numberOfFlats = 0
    #numberOfFloor;
    #lift;

    constructor(address, m2, cadastralReference, coordinates, isNew, basePrice, constructionDate, numberOfFloor, lift) {
        super(address, m2, cadastralReference, coordinates, isNew, basePrice, constructionDate)
        this.#numberOfFloor = numberOfFloor;
        this.#lift = lift;
        Flat.#numberOfFlats += 1;
    }

    static getNumberOfFlats() {
        return Flat.#numberOfFlats;
    }

    getPrice() {
        let currentPrice = super.getPrice();

        if (this.#numberOfFloor >= 3 && this.#lift) currentPrice *= 1.03;

        return currentPrice;
    }

}


class Local extends Property {

    #pictureWindows;
    #windows;
    #metalShutter;
    static #numberOfLocals = 0;

    constructor(address, m2, cadastralReference, coordinates, isNew, basePrice, constructionDate, pictureWindows, windows, metalShutter) {
        super(address, m2, cadastralReference, coordinates, isNew, basePrice, constructionDate)
        this.#pictureWindows = pictureWindows;
        this.#windows = windows;
        this.#metalShutter = metalShutter;
        Local.#numberOfLocals += 1;
    }

    getPrice() {
        let currentPrice = super.getPrice();
        let m2 = super.getM2();

        let valueFluctuation = 100;

        if (m2 > 50) valueFluctuation += 1;
        if (this.#pictureWindows < 2) valueFluctuation -= 2;
        if (this.#windows > 4) valueFluctuation += 2;

        let discount = (valueFluctuation - 100) / 100;

        discount += 1;

        currentPrice *= discount;

        return currentPrice;
    }

    static getNumberOfLocals() {
        return Local.#numberOfLocals;
    }
}

class ComercialLocal extends Local {
    #handicappedAccessible;

    static #numberOComercialfLocals = 0;
    constructor(address, m2, cadastralReference, coordinates, isNew, basePrice, constructionDate, pictureWindows, windows, metalShutter, handicappedAccessible) {
        super(address, m2, cadastralReference, coordinates, isNew, basePrice, constructionDate, pictureWindows, windows, metalShutter);
        this.#handicappedAccessible = handicappedAccessible;

        ComercialLocal.#numberOComercialfLocals += 1;
    }


    static getNumberOfComercialLocals() {
        return ComercialLocal.#numberOComercialfLocals;
    }

}

class RestorationLocal extends Local {
    #extractorHood;
    #coffeeMaker;
    #restorationFurniture;

    static #numberORestorationLocals = 0;
    constructor(address, m2, cadastralReference, coordinates, isNew, basePrice, constructionDate, pictureWindows, windows, metalShutter, extractorHood, coffeeMaker, restorationFurniture) {
        super(address, m2, cadastralReference, coordinates, isNew, basePrice, constructionDate, pictureWindows, windows, metalShutter);
        this.#extractorHood = extractorHood;
        this.#coffeeMaker = coffeeMaker;
        this.#restorationFurniture = restorationFurniture;

        RestorationLocal.#numberORestorationLocals += 1;
    }

    static getNumberOfRestorationLocals() {
        return RestorationLocal.#numberORestorationLocals;
    }

}

class IndustrialLocal extends Local {
    #loadingAndUnloading;
    #industrialLand;

    static #numberOfIndustrialLocals = 0;
    constructor(address, m2, cadastralReference, coordinates, isNew, basePrice, constructionDate, pictureWindows, windows, metalShutter, loadingAndUnloading, industrialLand) {
        super(address, m2, cadastralReference, coordinates, isNew, basePrice, constructionDate, pictureWindows, windows, metalShutter);
        this.#loadingAndUnloading = loadingAndUnloading;
        this.#industrialLand = industrialLand;

        IndustrialLocal.#numberOfIndustrialLocals += 1;
    }

    static getNumberOfIndustrialLocals() {
        return IndustrialLocal.#numberOfIndustrialLocals;
    }

    getPrice() {
        let currentPrice = super.getPrice();

        if (!this.#industrialLand) currentPrice *= 1.25;

        return currentPrice;
    }

}

property = new Property('C/ Aiguablava 35', 100, '1651653DSE16131', [54.22524, 36.84389], false, 100000, 1970);

flat = new Flat('C/ Borrás 41-43', 65, '6651611DSF1613', [95.142586, 12.659874], false, 100000, 2009, 3, true);

newLocal = new Local('C/ Borrás 41-43', 50, '6651611DSF1613', [95.142586, 12.659874], false, 100000, 2000, 1, 4, true);

restorationLocal = new RestorationLocal('C/ Borrás 41-43', 70, '6651611DSF1613', [95.142586, 12.659874], false, 100000, 2005, 2, 4, true, true, true, true);

comercialLocal = new ComercialLocal('C/ Borrás 41-43', 20, '6651611DSF1613', [95.142586, 12.659874], false, 100000, 1995, 2, 4, true, true);

industrialLocal = new IndustrialLocal('Pg Gracia 350', 500, '6651611DSF1613', [95.142586, 12.659874], false, 100000, 2015, 2, 5, true, true, false);



let text = 'Número de ';

console.log('Comprobando número de propiedades');
console.log(`${text}inmuebles: ${Property.getNumberOfProperties()}`);
console.log(`${text}pisos: ${Flat.getNumberOfFlats()}`);
console.log(`${text}locales: ${Local.getNumberOfLocals()}`);
console.log(`${text}locales comerciales: ${ComercialLocal.getNumberOfComercialLocals()}`);
console.log(`${text}locales de restauración: ${RestorationLocal.getNumberOfRestorationLocals()}`);
console.log(`${text}locales industriales: ${IndustrialLocal.getNumberOfIndustrialLocals()}`, '\n');

text = 'Valor del ';
console.log('Comprobando el valor de las propiedades');
console.log(`${text}inmueble: ${property.getPrice()}`);
console.log(`${text}piso: ${flat.getPrice()}`);
console.log(`${text}local: ${newLocal.getPrice()}`);
console.log(`${text}local comercial: ${comercialLocal.getPrice()}`);
console.log(`${text}local de restauración: ${restorationLocal.getPrice()}`);
console.log(`${text}local industrial: ${industrialLocal.getPrice()}`);
