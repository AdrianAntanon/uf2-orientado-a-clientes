class Milkshake {

    ingredients;
    myMilkshake;
    prices = [
        ['Freses', 1.50],
        ['Platan', 0.50],
        ['Mango', 2.50],
        ['Fruits del bosc', 1.00],
        ['Coco', 1.00],
        ['Poma', 1.75],
        ['Piña', 3.50]
    ];

    constructor(ingredient) {
        this.ingredients = ingredient
        let ingredientsOfMilkshake = [];

        this.prices.forEach(product => {
            this.ingredients.forEach(ingredient => {
                if (product[0].toLowerCase() === ingredient.toLowerCase()) {
                    ingredientsOfMilkshake.push(product);
                }
            });
        });

        this.myMilkshake = ingredientsOfMilkshake;
    }

    getCost() {
        const productCost = this.myMilkshake.map(product => product[1])
        const totalCost = productCost.reduce((accumulator, price) => accumulator + price, 0);

        return `Cost: ${totalCost}`;
    }

    getPrice() {
        let costOfSale = 1.5;
        let totalCost = this.getCost().split(" ").pop();

        totalCost *= costOfSale;
        totalCost = totalCost.toFixed(2);

        return `Preu: ${totalCost}`;
    }

    getDescription() {
        const nameOfProducts = this.myMilkshake.map(product => product[0])
        let sortedProducts = nameOfProducts.sort()
        let listOfProducts = sortedProducts.join(', ');
        let description = nameOfProducts.length === 1 ? `Batut de: ${listOfProducts}` : `Fusió de: ${listOfProducts}`;

        return description;
    }

}

myMilkshake = new Milkshake(['Platan', 'Fruits del bosc', 'Mango'])

console.log(myMilkshake.getCost());
console.log(myMilkshake.getPrice());
console.log(myMilkshake.getDescription()); 